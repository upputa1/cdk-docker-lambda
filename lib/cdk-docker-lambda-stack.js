/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */

import * as path from 'path';
import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import devTools from '@rsa-security/node-dev-tools';
import * as sns from '@aws-cdk/aws-sns';
import * as subscripition from '@aws-cdk/aws-sns-subscriptions';
import * as ssm from '@aws-cdk/aws-ssm';
import * as iam from '@aws-cdk/aws-iam';

const {
  dependenciesLayer,
  storeArnParameter,
  role,
  runtime
} = devTools.cdk.lambda;

const NAME = 'CdkDockerLambda';
const CONTROL_OUTBOUND_TOPIC = 'NWS-control-Outbound-Topic';

export class CdkDockerLambdaStack extends cdk.Stack {
  constructor(scope, id, props) {
    super(scope, id, props);

    const { namespace } = props;
    const { service } = props;

    const cdkRole = (stack, lambdaName) => {
      const pbArn = ssm.StringParameter.valueForStringParameter(stack, '/NWS/dev/permission-boundary-cba');

      return new iam.Role(stack, `${lambdaName}LambdaRole`, {
        assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
        roleName: `${namespace}-${service}-${lambdaName}Lambda-Role`,
        managedPolicies: [
          iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaBasicExecutionRole')
        ],
        permissionsBoundary: iam.ManagedPolicy.fromManagedPolicyArn(stack, `${lambdaName}PermissionsBoundary`, pbArn)
      });
    };

    const lambdaRole = cdkRole(this, NAME);
    //
    // const lambdaDependenciesLayer = dependenciesLayer(this, NAME, namespace, service);

    // Configure path to Dockerfile
    const dockerfile = path.join(__dirname, '../docker');
    console.log('docker region', process.env.AWS_REGION);

    const env = {
      NWS: 'DOCKER'
    };
    console.log('environment', env);

    // Create AWS Lambda function and push image to ECR
    const adminStackFunction = new lambda.DockerImageFunction(this, `${NAME}function`, {
      code: lambda.DockerImageCode.fromImageAsset(dockerfile, {
        buildArgs: {
          NPM_TOKEN: process.env.NPM_TOKEN,
          SERVICE_NAME: 'delete-me',
          SERVICE_VERSION: '1.1.0',
          AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
          AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
          AWS_REGION: process.env.AWS_REGION
        }
      }),
      role: lambdaRole,
      environment: env,
      memorySize: 1024,
      timeout: cdk.Duration.minutes(2)
    });


    const controlOutboundTopicArn = 'arn:aws:sns:us-east-1:642374541031:NWS-control-Outbound-Topic';
    // eslint-disable-next-line max-len
    // const controlOutboundTopicArn = `arn:aws:${process.env.AWS_REGION}:${process.env.AWS_ACCOUNT}:NWS-control-Outbound-Topic`;
    const entitlementsTopic = process.env.CONTROL_OUTBOUND_TOPIC || controlOutboundTopicArn;

    const controlTopic = sns.Topic.fromTopicArn(this, 'control-outbound-topic', entitlementsTopic);

    // const testStackFunction = new lambda.Function(this, `${NAME}Handler`, {
    //   runtime,
    //   code: lambda.Code.fromAsset('function'),
    //   handler: 'main.handler',
    //   role: lambdaRole,
    //   functionName: `${namespace}-${service}-${NAME}Lambda`,
    //   layers: [lambdaDependenciesLayer],
    //   environment: {
    //     ENVIRONMENT: props.environment
    //   }
    //
    // });
    controlTopic.addSubscription(new subscripition.LambdaSubscription(adminStackFunction));
  }


}
