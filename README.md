# cdk-docker-lambda

# NEXT STEPS!

So you have a new thing! Congrats! There's a few things left to do that `nwcli` could not do for you (yet).

Note: Steps 4+ assume the new lambda is tied to a gitlab repository. Some features of the project, like releasing the code, require it. If you do not have a repository set up and are just playing around, you can skip these steps. If this code is meant to land in GitLab eventually, you will need to return to these steps and complete them.

1. Run your unit tests. The initial drop of code includes some dummy code/tests that you will soon replace. `npm test` will execute those tests.
2. Deploy your Lambda to AWS. `npm run deploy`. To do this you will need to be SSO'd into AWS and have pasted the `export`s in the terminal window.
3. Run your integration tests (Lambda must be deployed to AWS) which tests your Lambda running in AWS. The initial drop of code has some of those tests too. `npm run it` will execute those tests.
4. Update `.npmrc` to include the Project ID of your GitLab repo. The ID can be found right at the top of the main page for the repo. For instance, the Project ID for [node-dev-tools](https://gitlab.com/rsa-security/netwitness-engineering/node-dev-tools) is `22126067`.
5. Update `package.json`'s `publishConfig` to change `PROJECT_ID` to your Project ID.
6. Add the `SEMANTIC_VERSIONING_TOKEN`, for details on how to do that, see [this MR](https://gitlab.com/rsa-security/netwitness-engineering/cloud-e2e/-/merge_requests/8) starting with the numbered steps in the 5th bullet.
7. Clean up this `README.md`! You will probably want to leave the stuff below this list.
8. Push all these changes to a branch of your repo and open an MR. This should kick off an MR build.
9. Make sure you have disabled squashing of commits in your repo. Also, disable merge commits. Fast-forward commits only. Squashing and merge commits will break the versioning/tagging/releasing.
10. Merge the MR once it passes. This should result in version 1.0.0 of your repo being published to the Archive
11. Start writing code!

# Local Setup

`./scripts/welcome.sh`

This will get libraries installed and get the right version of node.js set up. If you are changing node versions, you may need to open a new terminal window to have the node version change take effect.

# Tests

## Unit Tests

`npm test`

## Integration Tests

`npm run it`

# Stack Deployment

`deploy`, `undeploy` commands deploy, undeploy resources respectively with the prefix `NWS-${user}` where `${user}` is derived from AWS default profile user.

## Deploy

`npm run deploy`

## Undeploy

`npm run undeploy`

# Developing Locally with SAM

AWS SAM local can be used for local testing while developing.

Pre-requisites:

- Docker
- AWS CDK
- AWS SAM Cli

#### Steps

1. Deploy your lambda using `npm run deploy`. This is required for projects using lambda layer.
   `sam local` command downloads this layer from your default environment for any lambda invocations.

2. Generate the template using:

   `npm run synth`

3. To test any changes you have made locally, invoke the lambda

   `sam local invoke -t cdk.out/${NWS}-CdkDockerLambdaStack.template.json --event test/events/usersByPool.json`
