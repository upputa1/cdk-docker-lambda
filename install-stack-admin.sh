#!/usr/bin/env bash
mkdir "stack"
cd stack
npm init -y
echo @rsa-security:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
echo //gitlab.com/api/v4/packages/npm/:_authToken=${NPM_TOKEN} >>.npmrc
npm install @rsa-security/admin-services@1.1.12
#cp -r node_modules/@rsa-security/admin-services/ .
#rm -rf node_modules
