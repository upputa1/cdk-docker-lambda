/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */

const execSync = require('child_process').execSync;

exports.handler = async function(event, context) {
  console.log(event);
  console.log(event.tenantId);

  const params = 'tenantId=t101';
  console.log(params);
  const deploy = '. /opt/stack/deploy-cdk-stack.sh';
  const CMD = `${deploy}`;
  console.log('inside main handler', CMD);

  try {
    const result = execSync(CMD, { stdio: 'inherit' });
    console.log(result);
  } catch (e) {
    console.error('exec sync error', e);
  }
};
