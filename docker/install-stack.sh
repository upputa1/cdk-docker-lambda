#!/usr/bin/env bash
cd /opt/stack
echo "service args"
echo $1
echo $2
echo $3

npm init -y
echo @rsa-security:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
echo //gitlab.com/api/v4/packages/npm/:_authToken=${3} >>.npmrc
npm install @rsa-security/$1@$2
cp -rf node_modules/\@rsa-security/$1/* .
npm install
npm run pre-deploy
scriptDir="$(dirname "$0")"
echo $scriptDir

