#!/usr/bin/env node

/* Copyright (c) 2021 RSA Security LLC or its affiliates. All rights reserved. */

import * as cdk from '@aws-cdk/core';
import { CdkDockerLambdaStack } from '../lib/cdk-docker-lambda-stack';
const devTools = require('@rsa-security/node-dev-tools');
const cdkUtils = devTools.cdk.utils;

// apply tags to the stack
const app = new cdk.App();
(async() => {
  const environment = await cdkUtils.getEnvironment();
  const namespace = await cdkUtils.getNameSpace();

  // read configuration from `bin/deploy-config.json`
  const envConfig = await cdkUtils.getDeploymentConfig(__dirname);

  // apply tags to the stack
  await cdkUtils.applyTags(app, envConfig.tags);

  const theStack = new CdkDockerLambdaStack(app, `${namespace}-CdkDockerLambdaStack`, {
    environment,
    namespace,
    region: app.node.tryGetContext('region'),
    service: app.node.tryGetContext('service')
  });

  devTools.cdk.aspects.addStackAspects(theStack);
})();
