#!/usr/bin/env bash
rm -rf stack
mkdir "stack"
cd stack
npm init -y
echo @rsa-security:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
echo //gitlab.com/api/v4/packages/npm/:_authToken=${NPM_TOKEN} >>.npmrc
npm install @rsa-security/nws-analytics@1.3.2
cp node_modules/@rsa-security/nws-analytics/ .
npm install
cp ./../deploy-cdk-stack.sh .
cp ./../entry/main.js .

