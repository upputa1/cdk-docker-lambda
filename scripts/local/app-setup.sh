# For MAC users source in nvm. For windows users it is a no-op as nvm-windows is already available in the path
if [ "$(uname)" == "Darwin" ] || [ "$(uname)" == "Linux" ]
then
# source in nvm so it can be used
. ~/.nvm/nvm.sh
fi

CWD=$(pwd)
scriptDir="$(dirname $0)"
. $scriptDir/_util.sh

# run install on node build node utilities
info "Running install for all application node packages"
cd $scriptDir/..
npm install
cd $CWD

