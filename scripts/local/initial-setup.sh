# Convienence script to setup the environment
# sets up nvm and node

# Software versions kept in external file for repeated use
# this brings the following variables in
# NODE_VERSION
scriptDir="$(dirname "$0")"
source $scriptDir/local/versions
source $scriptDir/local/_util.sh

echo -e "\nInstalling the following libraries:\nnode.js: $NODE_VERSION"

# At present, the script only supports OS X and Windows
# Check if the user is on OS X
if [ "$(uname)" == "Darwin" ]
then
  # Checks for NVM global created in .bash_profile
  # if its not there, NVM hasn't been installed, so install it
  if [ -z ${NVM_DIR+x} ]
  then
    info "Detected OS X, Installing NVM"
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
    success "NVM installed!"
  else
    info "NVM already present on OS X, not installing"
  fi
  # source in nvm so it can be used in this script
  source ~/.nvm/nvm.sh
  # install the required node version and make that version the default
  nvm install $NODE_VERSION
  nvm alias default $NODE_VERSION
# User is on Linux
elif [ "$(uname)" == "Linux" ]
then
# Checks for NVM global created in .bash_profile
  # if its not there, NVM hasn't been installed, so install it
  if [ -z ${NVM_DIR} ]
  then
    info "Detected Linux, Installing NVM"
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
    success "NVM installed!"
  else
    info "NVM already present on Linux, not installing"
  fi
  # source in nvm so it can be used in this script
  source ~/.nvm/nvm.sh
  # install the required node version and make that version the default
  nvm install $NODE_VERSION
  nvm alias default $NODE_VERSION
# User is not on OS X, must be on Windows
else
  # Check if nvm-windows has been installed
  if [ -x "$(command -v nvm)" ]
  then
    success "Detected NVM for Windows"
    # install the required node version and make that version the default
    nvm install $NODE_VERSION
    nvm use $NODE_VERSION
  else
    red "NVM is not installed on this Windows"
    info "Please go to https://github.com/coreybutler/nvm-windows/releases and install NVM"
    exit 1
  fi
fi

# Yay!
success "Initial setup complete!"
